<?php
session_start();
?>

<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet" type="text/css" href="./style/index.css">
</head>

<body>

    <nav>
        <ul class="main-menu">
            <li><a href="index.php">Home</a></li>
            <li><a href="news.php">News</a></li>
            <li><a href="buy.php">Buy</a>
                <ul>
                    <li><a href="buy.php#residential">Residential</a></li>
                    <li><a href="buy.php#comercial">Comercial</a></li>
                </ul>
            </li>
            <li><a href="rent.php">Rent</a>
                <ul>
                    <li><a href="rent.php#residential">Residential</a></li>
                    <li><a href="rent.php#comercial">Comercial</a></li>
                </ul>
            </li>
            <li><a href="about-us.php">About Us</a></li>
            <li><a href="contact-us.php">Contact us</a></li>
            <?php
            if (isset($_SESSION['username']) && $_SESSION['userType'] === 'regular') {
            ?>
                <li><a href="book.php">Book</a></li>
            <?php
            } else if (isset($_SESSION['username']) && $_SESSION['userType'] === 'admin') {
            ?>
                <li><a href="admin.php">Admin</a></li>
            <?php
            }
            ?>
        </ul>
    </nav>

    <div id="residential" class="buy-residential-tag">Residential Apartments and Houses for Sale</div>

    <div class="buy-or-rent-article">
        <p class="buy-or-rent-title">Apartament 3 camere decomandate, Intre Lacuri</p>
        <div class="slidershow middle">
            <div class="slides">
                <input type="radio" name="r" id="r11" checked>
                <input type="radio" name="r" id="r12">
                <input type="radio" name="r" id="r13">
                <input type="radio" name="r" id="r14">
                <input type="radio" name="r" id="r15">
                <input type="radio" name="r" id="r16">

                <div class="slide show">
                    <img class="slide-img" src="./images/buy/residential/property1.1.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/residential/property1.2.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/residential/property1.3.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/residential/property1.4.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/residential/property1.5.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/residential/property1.6.jpg">
                </div>
            </div>
            <div class="slide-navigation">
                <label for="r11" class="bar"></label>
                <label for="r12" class="bar"></label>
                <label for="r13" class="bar"></label>
                <label for="r14" class="bar"></label>
                <label for="r15" class="bar"></label>
                <label for="r16" class="bar"></label>
            </div>
        </div>

        <div class="buy-or-rent-details">
            <p class="buy-or-rent-details">Tip Locuinta: Apartament</p>
            <p class="buy-or-rent-details">Locatie: Cluj-Napoca, Cluj</p>
            <p class="buy-or-rent-details">Zona: Intre Lacuri</p>
            <p class="buy-or-rent-details">Numar camere: 3</p>
            <p class="buy-or-rent-details">Numar bai: 2</p>
            <p class="buy-or-rent-details">Numar bucatarii: 1</p>
            <p class="buy-or-rent-details">Etaj: 4/4</p>
            <p class="buy-or-rent-details">Parcare: nu</p>
            <p class="buy-or-rent-details">An constructie: 1988</p>
            <p class="buy-or-rent-price">Pret de vanzare: 165.000€</p>
        </div>
    </div>

    <div class="buy-or-rent-article">
        <p class="buy-or-rent-title">Casa ultramoderna, finisata, cu garaj, Feleacu</p>
        <div class="slidershow middle">
            <div class="slides">
                <input type="radio" name="r" id="r21" checked>
                <input type="radio" name="r" id="r22">
                <input type="radio" name="r" id="r23">
                <input type="radio" name="r" id="r24">
                <input type="radio" name="r" id="r25">
                <input type="radio" name="r" id="r26">

                <div class="slide show">
                    <img class="slide-img" src="./images/buy/residential/property2.1.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/residential/property2.2.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/residential/property2.3.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/residential/property2.4.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/residential/property2.5.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/residential/property2.6.jpg">
                </div>
            </div>
            <div class="slide-navigation">
                <label for="r21" class="bar"></label>
                <label for="r22" class="bar"></label>
                <label for="r23" class="bar"></label>
                <label for="r24" class="bar"></label>
                <label for="r25" class="bar"></label>
                <label for="r26" class="bar"></label>
            </div>
        </div>

        <div class="buy-or-rent-details">
            <p class="buy-or-rent-details">Tip Locuinta: Casa</p>
            <p class="buy-or-rent-details">Locatie: Cluj-Napoca, Cluj</p>
            <p class="buy-or-rent-details">Zona: Feleacu</p>
            <p class="buy-or-rent-details">Numar camere: 4</p>
            <p class="buy-or-rent-details">Numar bai: 2</p>
            <p class="buy-or-rent-details">Numar bucatarii: 1</p>
            <p class="buy-or-rent-details">Etaj: 3 nvele (D + P + E)</p>
            <p class="buy-or-rent-details">Parcare: 2 garaje</p>
            <p class="buy-or-rent-details">An constructie: 2016</p>
            <p class="buy-or-rent-price">Pret de vanzare: 289.000€</p>
        </div>
    </div>

    <div id="comercial" class="buy-comercial-tag">Comercial Buildings for Sale</div>

    <div class="buy-or-rent-article">
        <p class="buy-or-rent-title">Spatiu comercial 132mp, zona str. Oasului</p>
        <div class="slidershow middle">
            <div class="slides">
                <input type="radio" name="r" id="r31" checked>
                <input type="radio" name="r" id="r32">
                <input type="radio" name="r" id="r33">

                <div class="slide show">
                    <img class="slide-img" src="./images/buy/comercial/property1.1.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/comercial/property1.2.jpg">
                </div>
                <div class="slide">
                    <img class="slide-img" src="./images/buy/comercial/property1.3.jpg">
                </div>
            </div>

            <div class="slide-navigation">
                <label for="r31" class="bar"></label>
                <label for="r32" class="bar"></label>
                <label for="r33" class="bar"></label>
            </div>
        </div>

        <div class="buy-or-rent-details">
            <p class="buy-or-rent-details">Tip Spatiu: Comercial</p>
            <p class="buy-or-rent-details">Locatie: Cluj-Napoca, Cluj</p>
            <p class="buy-or-rent-details">Zona: Primaria Iris</p>
            <p class="buy-or-rent-details">Suprafata construita: 145 m2</p>
            <p class="buy-or-rent-details">Numar incaperi: 14</p>
            <p class="buy-or-rent-details">Numar bai: 8</p>
            <p class="buy-or-rent-details">Numar terase: 3</p>
            <p class="buy-or-rent-details">Etaj: P+6</p>
            <p class="buy-or-rent-details">Parcare: 5000 euro + TVA pentru constructia parcarii</p>
            <p class="buy-or-rent-details">An constructie: 2022 - constructie in desfasurare</p>
            <p class="buy-or-rent-price">Pret de vanzare: 269.500€</p>
        </div>
    </div>

</body>

</html>