<?php
session_start();
?>

<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet" type="text/css" href="./style/index.css">
    <script src="./js/xml_fetch_data.js"></script>
</head>

<body>

    <nav>
        <ul class="main-menu">
            <li><a href="index.php">Home</a></li>
            <li><a href="news.php">News</a></li>
            <li><a href="buy.php">Buy</a>
                <ul>
                    <li><a href="buy.php#residential">Residential</a></li>
                    <li><a href="buy.php#comercial">Comercial</a></li>
                </ul>
            </li>
            <li><a href="rent.php">Rent</a>
                <ul>
                    <li><a href="rent.php#residential">Residential</a></li>
                    <li><a href="rent.php#comercial">Comercial</a></li>
                </ul>
            </li>
            <li><a href="about-us.php">About Us</a></li>
            <li><a href="contact-us.php">Contact us</a></li>
            <?php
            if (isset($_SESSION['username']) && $_SESSION['userType'] === 'regular') {
            ?>
                <li><a href="book.php">Book</a></li>
            <?php
            } else if (isset($_SESSION['username']) && $_SESSION['userType'] === 'admin') {
            ?>
                <li><a href="admin.php">Admin</a></li>
            <?php
            }
            ?>
        </ul>
    </nav>

    <div class="article">
        <div id="article1">
            <p id="title1" class="news-title"></p>
            <p id="paragraph1" class="news-paragraph"></p>
            <p><a id="href1">Click here to see the full article </a></p>
        </div>

        <div id="article2">
            <p id="title2" class="news-title"></p>
            <p id="paragraph2" class="news-paragraph"></p>
            <p><a id="href2">Click here to see the full article </a></p>
        </div>
    </div>
</body>

</html>