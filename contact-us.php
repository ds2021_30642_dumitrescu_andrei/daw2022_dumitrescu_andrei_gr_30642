<?php
session_start();
?>

<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet" type="text/css" href="./style/index.css">
</head>

<body>

    <nav>
        <ul class="main-menu">
            <li><a href="index.php">Home</a></li>
            <li><a href="news.php">News</a></li>
            <li><a href="buy.php">Buy</a>
                <ul>
                    <li><a href="buy.php#residential">Residential</a></li>
                    <li><a href="buy.php#comercial">Comercial</a></li>
                </ul>
            </li>
            <li><a href="rent.php">Rent</a>
                <ul>
                    <li><a href="rent.php#residential">Residential</a></li>
                    <li><a href="rent.php#comercial">Comercial</a></li>
                </ul>
            </li>
            <li><a href="about-us.php">About Us</a></li>
            <li><a href="contact-us.php">Contact us</a></li>
            <?php
            if (isset($_SESSION['username']) && $_SESSION['userType'] === 'regular') {
            ?>
                <li><a href="book.php">Book</a></li>
            <?php
            } else if (isset($_SESSION['username']) && $_SESSION['userType'] === 'admin') {
            ?>
                <li><a href="admin.php">Admin</a></li>
            <?php
            }
            ?>
        </ul>
    </nav>

    <div class="article">
        <div>
            <p class="contact-us-title">Contact us</p>
            <p class="contact-us-paragraph"><img class="contact-us-image" src="./images/email.png" />realestate@agency.com</p>
            <p class="contact-us-paragraph"><img class="contact-us-image" src="./images/phone.png" />(319) 588-3706</p>
            <p class="contact-us-paragraph">Send us an email</p>

            <form method="POST" action="http://127.0.0.1:5000/email">
                <label class="contact-us-sender-label">Enter your email address: </label>
                <textarea name="email_sender" id="email_sender" class="contact-us-sender-input" rows="1"></textarea>
                <textarea name="message_sent" class="contact-us-input" rows="15"></textarea>
                <button type="submit" class="contact-us-button" type="submit">Send the email! </button>
            </form>
        </div>
    </div>

</body>

</html>