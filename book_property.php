<?php
session_start();
?>

<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet" type="text/css" href="./style/index.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="./js/book_property.js"></script>
</head>

<body>

    <?php
    if (!isset($_SESSION['username']) || !isset($_SESSION['userType']) || $_SESSION['userType'] === 'admin') {
    ?>
        <div> PERMISSION RESTRICTED </div>
    <?php
    } else {
    ?>
        <nav>
            <ul class="main-menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="news.php">News</a></li>
                <li><a href="buy.php">Buy</a>
                    <ul>
                        <li><a href="buy.php#residential">Residential</a></li>
                        <li><a href="buy.php#comercial">Comercial</a></li>
                    </ul>
                </li>
                <li><a href="rent.php">Rent</a>
                    <ul>
                        <li><a href="rent.php#residential">Residential</a></li>
                        <li><a href="rent.php#comercial">Comercial</a></li>
                    </ul>
                </li>
                <li><a href="about-us.php">About Us</a></li>
                <li><a href="contact-us.php">Contact us</a></li>
                <?php
                if (isset($_SESSION['username']) && $_SESSION['userType'] === 'regular') {
                ?>
                    <li><a href="book.php">Book</a></li>
                <?php
                } else if (isset($_SESSION['username']) && $_SESSION['userType'] === 'admin') {
                ?>
                    <li><a href="admin.php">Admin</a></li>
                <?php
                }
                ?>
            </ul>
        </nav>

        <div class="book-title"></div>

        <div class="book-article">
            <p id="book-property-message" class="error-message"></p>
            <label class="label" for="location-id">ID:</label>
            <input class="input-book" id="location-id" readonly />
            <label class="label" for="description">Details: </label>
            <input class="input-book" id="description" readonly />
            <label class="label" for="price">Price:</label>
            <input class="input-book" id="price" readonly />
            <label class="label" for="hour">Select Hour:</label>
            <input class="input-book" id="hour" type="time" pattern="09:00" required />
            <label class="label" for="date">Select Date:</label>
            <input class="input-book" id="date" type="date" />

            <button class="book-property-button" id="book-button" onclick="onBookPress()">Book now</button>
        </div>
    <?php
    }
    ?>

</body>

</html>