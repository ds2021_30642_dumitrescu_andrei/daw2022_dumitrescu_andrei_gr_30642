<?php
session_start();
?>

<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet" type="text/css" href="./style/index.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="./js/admin-see-views.js"></script>
</head>

<body>

    <?php
    if (!isset($_SESSION['username']) || !isset($_SESSION['userType']) ||  $_SESSION['userType'] === 'regular') {
    ?>
        <div> PERMISSION RESTRICTED </div>
    <?php
    } else {
    ?>
        <nav>
            <ul class="main-menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="news.php">News</a></li>
                <li><a href="buy.php">Buy</a>
                    <ul>
                        <li><a href="buy.php#residential">Residential</a></li>
                        <li><a href="buy.php#comercial">Comercial</a></li>
                    </ul>
                </li>
                <li><a href="rent.php">Rent</a>
                    <ul>
                        <li><a href="rent.php#residential">Residential</a></li>
                        <li><a href="rent.php#comercial">Comercial</a></li>
                    </ul>
                </li>
                <li><a href="about-us.php">About Us</a></li>
                <li><a href="contact-us.php">Contact us</a></li>
                <?php
                if (isset($_SESSION['username']) && $_SESSION['userType'] === 'regular') {
                ?>
                    <li><a href="book.php">Book</a></li>
                <?php
                } else if (isset($_SESSION['username']) && $_SESSION['userType'] === 'admin') {
                ?>
                    <li><a href="admin.php">Admin</a></li>
                <?php
                }
                ?>
            </ul>
        </nav>

        <div class="book-title">Check all the view bookings for a property</div>

        <div class="book-article">
            <p class="admin-article-title">Check all the view bookings for a renting property</p>

            <table id="book-table-renting" class="book-table">
                <thead>
                    <th></th>
                    <th>Details</th>
                    <th>Type</th>
                    <th>Price</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="book-article">
            <p class="admin-article-title">Check all the view bookings for a selling property</p>

            <table id="book-table-buying" class="book-table">
                <thead>
                    <th></th>
                    <th>Details</th>
                    <th>Type</th>
                    <th>Price</th>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    <?php
    }
    ?>

</body>

</html>