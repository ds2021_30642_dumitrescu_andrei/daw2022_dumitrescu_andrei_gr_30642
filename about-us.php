<?php
session_start();
?>

<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet" type="text/css" href="./style/index.css">
</head>

<body>

    <nav>
        <ul class="main-menu">
            <li><a href="index.php">Home</a></li>
            <li><a href="news.php">News</a></li>
            <li><a href="buy.php">Buy</a>
                <ul>
                    <li><a href="buy.php#residential">Residential</a></li>
                    <li><a href="buy.php#comercial">Comercial</a></li>
                </ul>
            </li>
            <li><a href="rent.php">Rent</a>
                <ul>
                    <li><a href="rent.php#residential">Residential</a></li>
                    <li><a href="rent.php#comercial">Comercial</a></li>
                </ul>
            </li>
            <li><a href="about-us.php">About Us</a></li>
            <li><a href="contact-us.php">Contact us</a></li>
            <?php
            if (isset($_SESSION['username']) && $_SESSION['userType'] === 'regular') {
            ?>
                <li><a href="book.php">Book</a></li>
            <?php
            } else if (isset($_SESSION['username']) && $_SESSION['userType'] === 'admin') {
            ?>
                <li><a href="admin.php">Admin</a></li>
            <?php
            }
            ?>
        </ul>
    </nav>

    <div class="article">
        <div>
            <p class="about-us-title">About us</p>
            <p class="about-us-second-title">OUR MISSION STATEMENT IS “WE DON’T JUST SELL HOUSES, WE DELIVER DREAMS”</p>
            <p class="about-us-paragraph">With the ever changing climate we strive to bring first class services which include offices in all major cities. Valuations, After Sales and Customer Service with Greek, English, Russian, Romanian, German, Polish, Latvian & Ukrainian speaking property negotiators.</p>
            <p class="about-us-paragraph">We aim to give a personal and caring service to all to fulfil your dream.</p>
            <p class="about-us-paragraph">Love where you live. Enjoy browsing the website and get in touch for the property of your dreams.</p>
            <p class="about-us-paragraph">To get in touch with us, click on the 'Contact us' tab.</p>
        </div>
    </div>

</body>

</html>