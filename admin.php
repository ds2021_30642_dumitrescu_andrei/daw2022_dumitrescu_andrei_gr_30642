<?php
session_start();
?>

<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet" type="text/css" href="./style/index.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="./js/adminpage.js"></script>
</head>

<body>

    <?php
    if (!isset($_SESSION['username']) || !isset($_SESSION['userType']) ||  $_SESSION['userType'] === 'regular') {
    ?>
        <div> PERMISSION RESTRICTED </div>
    <?php
    } else {
    ?>
        <nav>
            <ul class="main-menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="news.php">News</a></li>
                <li><a href="buy.php">Buy</a>
                    <ul>
                        <li><a href="buy.php#residential">Residential</a></li>
                        <li><a href="buy.php#comercial">Comercial</a></li>
                    </ul>
                </li>
                <li><a href="rent.php">Rent</a>
                    <ul>
                        <li><a href="rent.php#residential">Residential</a></li>
                        <li><a href="rent.php#comercial">Comercial</a></li>
                    </ul>
                </li>
                <li><a href="about-us.php">About Us</a></li>
                <li><a href="contact-us.php">Contact us</a></li>
                <?php
                if (isset($_SESSION['username']) && $_SESSION['userType'] === 'regular') {
                ?>
                    <li><a href="book.php">Book</a></li>
                <?php
                } else if (isset($_SESSION['username']) && $_SESSION['userType'] === 'admin') {
                ?>
                    <li><a href="admin.php">Admin</a></li>
                <?php
                }
                ?>
            </ul>
        </nav>

        <div id="residential" class="rent-residential-tag"></div>

        <div class="buy-or-rent-article">
            <p class="admin-page-title">Admin Menu</p>
            <button class="admin-page-button" onclick="addPropertyPressed()">Add Property</button>
            <button class="admin-page-button" onclick="seeViewsPressed()">See Views</button>
        </div>
    <?php
    }
    ?>

</body>

</html>