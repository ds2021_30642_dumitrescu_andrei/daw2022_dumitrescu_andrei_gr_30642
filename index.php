<?php
session_start();
?>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="./style/index.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="./js/homepage.js"></script>
</head>

<body>

    <nav>
        <ul class="main-menu">
            <li><a href="index.php">Home</a></li>
            <li><a href="news.php">News</a></li>
            <li><a href="buy.php">Buy</a>
                <ul>
                    <li><a href="buy.html#residential">Residential</a></li>
                    <li><a href="buy.html#comercial">Comercial</a></li>
                </ul>
            </li>
            <li><a href="rent.html">Rent</a>
                <ul>
                    <li><a href="rent.php#residential">Residential</a></li>
                    <li><a href="rent.php#comercial">Comercial</a></li>
                </ul>
            </li>
            <li><a href="about-us.php">About Us</a></li>
            <li><a href="contact-us.php">Contact us</a></li>
            <?php
            if (isset($_SESSION['username']) && $_SESSION['userType'] === 'regular') {
            ?>
                <li><a href="book.php">Book</a></li>
            <?php
            } else if (isset($_SESSION['username']) && $_SESSION['userType'] === 'admin') {
            ?>
                <li><a href="admin.php">Admin</a></li>
            <?php
            }
            ?>
        </ul>
    </nav>

    <?php
    if (isset($_SESSION['username'])) {
    ?>
        <div class="welcome-text">Buying or renting a property has never been easier</div>
    <?php  } else { ?>
        <div class="register">
            <a class="register-header">REGISTER</a>
            <input class="first-input" id="username-register" placeholder="Enter your username..." />
            <input class="input" id="email-register" placeholder="Enter your email..." />
            <input class="input" id="password-register" placeholder="Enter your password..." type="password" />

            <button class="home-button-register" id="register-button" onclick="onRegisterPress()">REGISTER</button>
            <p class="error-or-success-message" id="error-or-success-message-register"></p>
        </div>

        <div class="login">
            <a class="login-header">LOG-IN</a>
            <input class="first-input" id="username-login" placeholder="Enter your username..." />
            <input class="input" id="password-login" placeholder="Enter your password..." type="password" />

            <button class="home-button-login" id="login-button" onclick="onLoginPress()">LOG-IN</button>
            <p class="error-or-success-message" id="error-or-success-message-login"></p>
        </div>
    <?php } ?>

</body>

</html>