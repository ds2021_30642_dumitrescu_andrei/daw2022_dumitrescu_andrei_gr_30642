let description = ""
let type = ""
let saleOrRent = ""
let price = ""

const initializeParams = () => {
    description = document.getElementById('description').value
    type = document.getElementById('type').value
    saleOrRent = document.getElementById('sale-or-rent').value
    price = document.getElementById('price').value
}


const onAddPropertyPressed = () => {
    initializeParams();

    const postData = {
        description: description,
        type: type,
        saleOrRent: saleOrRent,
        price: price
    }

    console.log(postData)

    $.ajax({
        type: "POST",
        url: "http://localhost:8000/api/create-location",
        data: JSON.stringify(postData),
        dataType:'json',
        CrossDomain: true,
        contentType: 'application/json',
        success: function(result){
            if (result.status === '200') { 
                printSuccessMessage(result.response);
            } else {
                console.log(result)
                printErrorMessage(result.response);
            }
            
        },
        error: function (error) { 
            console.log(error)
        }
    });
}

const printSuccessMessage = (message) => {
    let paragraph = document.getElementById('admin-error-message')
    paragraph.innerHTML = message
    paragraph.style.color = "green"
}

const printErrorMessage = (message) => {
    let paragraph = document.getElementById('admin-error-message')
    paragraph.innerHTML = message
    paragraph.style.color = "red"
}