let urlString = window.location.href
let url = new URL(urlString)
let urlId = url.searchParams.get("id")

let id = ""
let description = ""
let price = ""
let hour = ""
let date = ""


const onBookPress = () => {
    initializeParams();

    const postData = {
        location_id: id,
        hour: hour,
        date: date
    }

    console.log(postData)

    console.log(postData)
    $.ajax({
        type: "POST",
        url: "http://localhost:8000/api/create-location-view",
        data: JSON.stringify(postData),
        dataType:'json',
        CrossDomain: true,
        contentType: 'application/json',
        success: function(result){
            if (result.status === '200') { 
                printSuccessMessage(result.response);
            } else {
                printErrorMessage(result.response);
            }
            
        },
        error: function (error) { 
            console.log(error)
        }
    });

}
const initializeParams = () => {
    id = document.getElementById('location-id').value
    description = document.getElementById('description').value
    price = document.getElementById('price').value
    hour = document.getElementById('hour').value
    date = document.getElementById('date').value
}

const printSuccessMessage = (message) => {
    let paragraph = document.getElementById('book-property-message')
    paragraph.innerHTML = message
    paragraph.style.color = "green"
}

const printErrorMessage = (message) => {
    let paragraph = document.getElementById('book-property-message')
    paragraph.innerHTML = message
    paragraph.style.color = "red"
}

const populateInputs = (result) => {
    let idInput = document.getElementById('location-id')
    let descriptionInput = document.getElementById('description')
    let priceInput = document.getElementById('price')

    idInput.value = result.id
    descriptionInput.value = result.description
    priceInput.value = result.price
}

$.ajax({
    type: "GET",
    url: "http://localhost:8000/api/location/" + urlId,
    dataType:'json',
    CrossDomain: true,
    success: function(result){
        if (result === null) { 
            printErrorMessage("Property doesn't exist!")
        } else {
            populateInputs(result)
        }
    },
    error: function (error) { 
        console.log(error)
    }
});