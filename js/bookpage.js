const populateTables = (data) => {

    data.forEach(element => {
        let tBody = ''
        if (element.isForSale) { 
            tBody = document.getElementById('book-table-buying').getElementsByTagName('tbody')[0];
        } else {
            tBody = document.getElementById('book-table-renting').getElementsByTagName('tbody')[0];
        }
     
        let newRow = tBody.insertRow();

        let newLinkCell = newRow.insertCell();
        let newDetailsCell = newRow.insertCell();
        let newTypeCell = newRow.insertCell();
        let newPriceCell = newRow.insertCell();

        let link = document.createElement("a")
        let url = "book_property.php?id=" + element.id
        link.setAttribute("href", url)
        link.className = "book-now-href"

        let newSelectText = document.createTextNode('Book');
        let newDetailsText = document.createTextNode(element.description)
        let newTypeText = document.createTextNode(element.type)
        let newPriceText = document.createTextNode(element.price)

        link.appendChild(newSelectText)
            
        newLinkCell.appendChild(link)
        newDetailsCell.appendChild(newDetailsText)
        newTypeCell.appendChild(newTypeText)
        newPriceCell.appendChild(newPriceText)
    });
}

$.ajax({
    type: "GET",
    url: "http://localhost:8000/api/locations",
    dataType:'json',
    CrossDomain: true,
    success: function(result){
        populateTables(result);
    },
    error: function (error) { 
        alert.console.log(error)
    }
});