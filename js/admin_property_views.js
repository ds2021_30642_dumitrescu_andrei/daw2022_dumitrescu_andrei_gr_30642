let urlString = window.location.href
let url = new URL(urlString)
let urlId = url.searchParams.get("id")

document.getElementById('property-location-id').value = urlId

let id = urlId
let date = ""

const setErrorMessage = (message) => { 
    let paragraph = document.getElementById('error-message')
    paragraph.innerHTML = message
    paragraph.style.color = "red"
}

const initializeDate = () => {
    date = document.getElementById('property-date').value
}

const createTable = (data) => {
    
    let exDivDate = document.getElementById("paragraph-date")
    let exDivHour = document.getElementById("paragraph-hour")

    if (exDivDate !== null && exDivHour !== null) {
        exDivDate.remove()
        exDivHour.remove()
    }

    let divHour = document.createElement("div")
    let divDate = document.createElement("div")
    
    divHour.className = "paragraph-hour"
    divDate.className = "paragraph-date"

    divHour.id = "paragraph-hour"
    divDate.id = "paragraph-date"

    document.getElementById("admin-views-article").appendChild(divHour)
    document.getElementById("admin-views-article").appendChild(divDate)

    data.forEach(element => {

        let paragraphDate = document.createElement("p")
        let paragraphHour = document.createElement("p")

        paragraphDate.innerText = element.date
        paragraphHour.innerText = element.hour

        paragraphDate.className = "admin-views-date"
        paragraphHour.className = "admin-views-hour"
        paragraphDate.id = "views-paragraph-date"
        paragraphHour.id = "views-paragraph-hour"

        divDate.appendChild(paragraphDate)
        divHour.appendChild(paragraphHour)

    });
}

const onViewBookingsPressed = () => {
    initializeDate()
    
    const postData = { 
        id: id,
        date: date
    }

    $.ajax({
        type: "POST",
        url: "http://localhost:8000/api/get-location-views",
        data: JSON.stringify(postData),
        dataType:'json',
        CrossDomain: true,
        contentType: 'application/json',
        success: function(result){

            console.log(result)

            if (result.status === undefined) { 

                if (result.length == 0) {
                    setErrorMessage("No views for this date!")
                } else {
                    setErrorMessage("")
                    createTable(result)
                }
            } else {
                console.log("a")
                setErrorMessage(result.response)
            }
        },
        error: function (error) { 
            alert.console.log(error)
        }
    });
}