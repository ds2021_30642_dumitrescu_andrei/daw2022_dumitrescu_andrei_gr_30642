let usernameRegister = ''
let emailRegister = ''
let passwordRegister = ''
let usernameLogIn = ''
let passwordLogIn = ''

const initializeRegisterParams = () => {
    usernameRegister = document.getElementById('username-register').value
    emailRegister = document.getElementById('email-register').value
    passwordRegister = document.getElementById('password-register').value
}

const initializeLogInParams = () => {
    usernameLogIn = document.getElementById('username-login').value
    passwordLogIn = document.getElementById('password-login').value
}


const successMessage = (message, source) => {
    let paragraph = '';
    if (source === 'log-in') { 
        paragraph = document.getElementById('error-or-success-message-login')
    } else {
        paragraph = document.getElementById('error-or-success-message-register')
    }    
    
    paragraph.innerHTML = message
    paragraph.style.color = 'green'
    paragraph.style.fontWeight = 'bold'
}

const errorMessage = (message, source) => { 
    let paragraph = '';
    if (source === 'log-in') { 
        paragraph = document.getElementById('error-or-success-message-login')
    } else {
        paragraph = document.getElementById('error-or-success-message-register')
    }
    
    paragraph.innerHTML = message
    paragraph.style.color = 'red'
    paragraph.style.fontWeight = 'bold'
}


const onRegisterPress = () => { 
    initializeRegisterParams()

    const postData = {
        name: usernameRegister,
        email: emailRegister,
        password: passwordRegister
    }

    $.ajax({
        type: "POST",
        url: "http://localhost:8000/api/create-user",
        data: JSON.stringify(postData),
        dataType:'json',
        CrossDomain: true,
        contentType: 'application/json',
        success: function(result){
            if (result.status === '200') { 
                successMessage(result.response, 'register');
            } else {
                errorMessage(result.response, 'register');
            }
            
        },
        error: function (error) { 
            alert.console.log(error)
        }
    });
}

const onLoginPress = () => {
    initializeLogInParams()

    const postData = {
        username: usernameLogIn,
        password: passwordLogIn
    }

    $.ajax({
        type: "POST",
        url: "http://localhost:8000/api/log-in",
        data: JSON.stringify(postData),
        dataType:'json',
        CrossDomain: true,
        contentType: 'application/json',
        success: function(result){
            if (result.status === '200') { 
                $.post( "set_session.php", { username: usernameLogIn, userType: result.userType } );
                window.location.href="http://localhost/realestate-frontend/"
            } else {
                errorMessage(result.response, 'log-in');
            }
        },
        error: function (error) { 
            console.log(error)
        }
    });
}