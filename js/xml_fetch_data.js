let xmlContent = "";

fetch('./xml/articles.xml').then((response) => {
    response.text().then((xml) => {
        xmlContent = xml;

        let parser = new DOMParser();
        let xmlDOM = parser.parseFromString(xmlContent, 'application/xml');
        let articlesFromXML = xmlDOM.querySelectorAll('article');
        let index = 0;
        
        console.log(articlesFromXML);

        articlesFromXML.forEach(articleFromXML => {
            let title = document.getElementById('title' + (index + 1));
            let paragraph = document.getElementById('paragraph' + (index + 1));
            let href = document.getElementById('href' + (index + 1));

            title.innerText = articleFromXML.children[0].innerHTML;
            paragraph.innerText = articleFromXML.children[1].innerHTML;
            href.href = articleFromXML.children[2].innerHTML;

            index++;
        }) 
    });
});