<?php
session_start();
?>

<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet" type="text/css" href="./style/index.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="./js/admin-add-property.js"></script>
</head>

<body>

    <?php
    if (!isset($_SESSION['username']) || !isset($_SESSION['userType']) ||  $_SESSION['userType'] === 'regular') {
    ?>
        <div> PERMISSION RESTRICTED </div>
    <?php
    } else {
    ?>
        <nav>
            <ul class="main-menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="news.php">News</a></li>
                <li><a href="buy.php">Buy</a>
                    <ul>
                        <li><a href="buy.php#residential">Residential</a></li>
                        <li><a href="buy.php#comercial">Comercial</a></li>
                    </ul>
                </li>
                <li><a href="rent.php">Rent</a>
                    <ul>
                        <li><a href="rent.php#residential">Residential</a></li>
                        <li><a href="rent.php#comercial">Comercial</a></li>
                    </ul>
                </li>
                <li><a href="about-us.php">About Us</a></li>
                <li><a href="contact-us.php">Contact us</a></li>
                <?php
                if (isset($_SESSION['username']) && $_SESSION['userType'] === 'regular') {
                ?>
                    <li><a href="book.php">Book</a></li>
                <?php
                } else if (isset($_SESSION['username']) && $_SESSION['userType'] === 'admin') {
                ?>
                    <li><a href="admin.php">Admin</a></li>
                <?php
                }
                ?>
            </ul>
        </nav>

        <div id="residential" class="rent-residential-tag"></div>

        <div class="buy-or-rent-article">
            <p class="admin-page-title">Add Article</p>
            <p class="admin-error-message" id="admin-error-message"></p>
            <label class="label" for="description">Description:</label>
            <input class="input-book" id="description" placeholder="Enter a description..." />
            <label class="label" for="type">Type </label>
            <input class="input-book" id="type" placeholder="Enter a type (only residential or comercial is allowed)..." />
            <label class="label" for="sale-or-rent">Sale or Rent:</label>
            <input class="input-book" id="sale-or-rent" placeholder="Enter if the property is for sale or rent (only rent or sale is allowed)..." />
            <label class="label" for="price">Price:</label>
            <input class="input-book" id="price" placeholder="Enter a price..." />
            <button class="book-property-button" id="book-button" onclick="onAddPropertyPressed()">Add Property</button>
        </div>
    <?php
    }
    ?>

</body>

</html>